package controllers

import models.Transaction.Transaction
import play.api.libs.json._
import play.api.mvc._

class Application extends Controller {

  def getTransaction(id: Long) = Action {
    models.Transaction.find(id).map { t => Ok(Json.toJson(t)) }.getOrElse(NotFound)
  }

  def listTransactionIdsForType(typeName: String) = Action { Ok( Json.toJson(
      models.Transaction.listTransactionsOfType(typeName)
  )) }

  def getTransactionTotalAmount(id: Long) = Action {
    Ok(Json.obj("sum" -> models.Transaction.totalAmount(id))) }

  def recordTransaction(id: Long) = Action(BodyParsers.parse.json) { request =>
    val t = request.body.validate[Transaction]
    t.fold(
      errors => {
        // There's a warning here, but it's not clear if toFlatJson
        // should really have been deprecated
        // https://github.com/playframework/playframework/issues/5531
        BadRequest(Json.obj("status" -> "Invalid transaction JSON", "message" -> JsError.toFlatJson(errors)))
      },
      transaction => {
        models.Transaction.recordTransaction(id, transaction).fold(
          error => BadRequest(Json.obj("status" -> error)),
          done => Ok(Json.obj("status" -> "ok"))
        )
      }
    )
  }
}
