package models

import java.util.concurrent.{ConcurrentMap, ConcurrentHashMap}
import play.api.libs.json.Json

object Transaction {

  /** spec says "amount" should be double, but I reject that. No need to introduce floating point madness.
    *
    * "parent_id" should be camel-case, but having to mess with the JSON marshalling not worth it.
    *
    * `type`: http://stackoverflow.com/a/34018822/14955
    */
  case class Transaction(amount: BigDecimal, `type`: String, parent_id: Option[Long])

  implicit val transactionWrites = Json.writes[Transaction]
  implicit val transactionReads = Json.reads[Transaction]

  // in-memory "database" of transactions, a collection of ConcurrentHashMaps
  private val byId = new ConcurrentHashMap[Long, Transaction]()

  private val byType = new ConcurrentHashMap[String, List[Long]]()

  private val totalAmounts = new ConcurrentHashMap[Long, BigDecimal]()


  // helper function to do atomic replace of an updated hashMap key
  def atomicUpdate[K,V](map: ConcurrentMap[K, V], key: K, updater: Function1[V, V]): Unit = while (true){
    val old = map.get(key)
    val updated = updater(old)
    if (map.replace(key, old, updated)) return
  }

  val okay = Right(true)

  val zero = BigDecimal.valueOf(0)



  def recordTransaction(id: Long, t: Transaction)  = {
      // concurrent inserts made possible by ConcurrentHashMap, no synchronization needed

    val checkParentId = t.parent_id match {
      case None => okay
      case Some(parentId) if totalAmounts.containsKey(parentId) => okay
      case _ => Left("parent transaction is missing")
    }

    def addToTypes() : Unit =  {
      byType.putIfAbsent(t.`type`, Nil)
      atomicUpdate[String, List[Long]](byType, t.`type`, old => id :: old)
    }

    // recursively adds it to all parents, too
    def addToTotalAmounts(id: Long, parentId: Option[Long], amount: BigDecimal) : Unit = {
      totalAmounts.putIfAbsent(id, zero)
      parentId match {
        case None => okay
        case Some(pId) => {
          val parent = byId.get(pId)  // we have already checked that this exists
          addToTotalAmounts(pId, parent.parent_id, amount)
        }
      }
      atomicUpdate[Long, BigDecimal](totalAmounts, id, _ + amount)

    }



    checkParentId match {
        case error@Left(_) => error
        case okay => {
          val recordedValue = byId.computeIfAbsent(id,
            // unfortunately Scala does not yet bridge to Java8 Function, so we need to drop down into Java-Land here
            new java.util.function.Function[Long, Transaction]() {
              override def apply(id: Long): Transaction = {
                // need to insert in the dependent maps as well
                addToTypes
                addToTotalAmounts(id, t.parent_id, t.amount)
                t
              }
            })
          if (recordedValue.ne(t)) Left("this transaction already exists")
          else okay
        }
      }


  }

  // no synchronization needed for queries !
  def find(id: Long) = Option(byId.get(id))

  def listTransactionsOfType(typeName: String) = byType.getOrDefault(typeName, Nil)

  def totalAmount(rootId: Long) = Option(totalAmounts.get(rootId)).getOrElse(zero)

}
