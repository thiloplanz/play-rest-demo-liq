Simple REST API in Play
-----------------------

This is a demonstration of a minimal REST API implemented in Play Scala.

Notes:
-------------

* The "database" is just a couple of in-memory ConcurrentHashMap (one for each query type)
* This allows for non-blocking queries and even a high decree of concurrent updates (i.e. inserts, there is only PUT)
* The data structure is designed to allow for all queries to be resolved in a single hash lookup (basically O(1) )
* Compared to a simpler datastructure such as a list, storage requirements are higher, but still O(N)
* using ConcurrentHashMap requires dropping down into Java API more than I would have liked (especially for "computeIfAbsent",
  which is not nicely bridged from Scala yet).
* The spec calls for "amount" to be a double, but I reject that. No need to introduce floating point madness.

